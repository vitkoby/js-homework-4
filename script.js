/* Ответы на теоритические вопросы:
1. Функции дают возможность выполнять код для разных условий за счет аргументов и параметров
таким образом сильно сокращают количество кода.
2. За счет аргументов функции мы можем проверить сразу несколько значений в функции
выполнив это при минимальном количсестве кода.
3. Оператор return возвращает значение функции и прирывает ее работу. */

const minicalc = document.querySelector('#minicalc');
const inputFirstNumber = document.querySelector('#first-number'); //minicalc.querySelector('first-number'); по #id
const inputSecondNumber = document.getElementById('second-number'); //minicalc.querySelector('second-number'); по классу
const inputOperation = document.querySelector('#operation');
const calcButton = document.getElementById('button');
const result = document.getElementById('result');


calcButton.addEventListener('click', () => {
    if (!!inputFirstNumber.value && !!inputSecondNumber.value && !!inputOperation.value) {
        const operation = {
            '+': Number(inputFirstNumber.value) + Number(inputSecondNumber.value),
            '-': Number(inputFirstNumber.value) - Number(inputSecondNumber.value),
            '*': Number(inputFirstNumber.value) * Number(inputSecondNumber.value),
            '/': Number(inputFirstNumber.value) / Number(inputSecondNumber.value),
        };
        const res = operation[inputOperation.value];
        result.innerText = `Результат ${res}`;
    } else {
        alert('Вам необходимо заполнить все поля');
    }
});



